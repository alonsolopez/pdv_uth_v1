﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Zen.Barcode.Code11BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code11WithChecksum;
            pictureBox1.Image = barcode.Draw("7501008433676", 50);
            Zen.Barcode.Code128BarcodeDraw barcode2 = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
            pictureBox2.Image = barcode2.Draw("7501008433676", 50);
            Zen.Barcode.Code25BarcodeDraw barcode3 = Zen.Barcode.BarcodeDrawFactory.Code25StandardWithChecksum;
            pictureBox3.Image = barcode3.Draw("7501008433676", 50);
            Zen.Barcode.Code39BarcodeDraw barcode4 = Zen.Barcode.BarcodeDrawFactory.Code39WithChecksum;
            pictureBox4.Image = barcode4.Draw("7501008433676", 50);

            Zen.Barcode.Code93BarcodeDraw barcode5 = Zen.Barcode.BarcodeDrawFactory.Code93WithChecksum;
            pictureBox5.Image = barcode5.Draw("7501008433676", 50);
            Zen.Barcode.CodeEan13BarcodeDraw barcode6 = Zen.Barcode.BarcodeDrawFactory.CodeEan13WithChecksum;
            pictureBox6.Image = barcode6.Draw("978841719900", 50);
            Zen.Barcode.CodeEan8BarcodeDraw barcode7 = Zen.Barcode.BarcodeDrawFactory.CodeEan8WithChecksum;
           // pictureBox7.Image = barcode7.Draw("7501008433676", 50);
            Zen.Barcode.CodePdf417BarcodeDraw barcode8 = Zen.Barcode.BarcodeDrawFactory.CodePdf417;
            pictureBox8.Image = barcode8.Draw("7501008433676", 100);
            Zen.Barcode.CodeQrBarcodeDraw barcode9 = Zen.Barcode.BarcodeDrawFactory.CodeQr;
            pictureBox9.Image = barcode9.Draw("7501008433676", 100);
        }
    }
}
