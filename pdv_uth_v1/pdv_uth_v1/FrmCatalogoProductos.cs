﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1.productos;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoProductos : Form
    {
        //var de producto
        Producto prod = new Producto();

        public FrmCatalogoProductos()
        {
            InitializeComponent();
        }

      

        private void iconBtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCatalogoProductos_Load(object sender, EventArgs e)
        {
            //set del combo
            comboUnidadDeMedida.SelectedItem = comboUnidadDeMedida.Items[0];
        }

        private void btnCargarImagen_Click_1(object sender, EventArgs e)
        {
            
        }

        private void iconBtnGuardar_Click(object sender, EventArgs e)
        {
            //cargar todos los campos en producto
            prod = new Producto();
            prod.Nombre = txtNombre.Text;
            prod.Descripcion = txtDescripcion.Text;
            prod.Precio = double.Parse(txtPrecio.Text);
            prod.CodigoDeBarras = txtCodBarras.Text;
            prod.Imagen = lblImagenAGuardar.Text;
            prod.UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), comboUnidadDeMedida.SelectedItem.ToString());
            prod.EsPerecedero = checkBoxEsPereceredo.Checked;
            //hacer el insert con m{etodo alta()
            if (prod.alta())
            {
                // obtener el dir de la app
                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\Productos\\";
                //guardar la imagen
                picBoxProducto.Image.Save(dir + lblImagenAGuardar.Text);
                //notificar
                MessageBox.Show("EL producto <" + txtNombre.Text + "> se ha almacenado.", "Nuevo Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Error, no se almacenó producto. " + Producto.msgError);
           
        }

        private void icoBtnCargarImagen_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogCargarImagen.Filter = "Image Files|*.png;";
            openFileDialogCargarImagen.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff";
            openFileDialogCargarImagen.Title = "Abrir Imagen para Producto";
            openFileDialogCargarImagen.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (openFileDialogCargarImagen.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogCargarImagen.FileName;
                //se carga archivo por su nombre al picBox
                picBoxProducto.Image = Image.FromFile(openFileDialogCargarImagen.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogCargarImagen.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension=lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"prod_{0}."+extension, DateTime.Now.Ticks);
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                Zen.Barcode.CodeEan13BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.CodeEan13WithChecksum;
                //le quitamos el último caracter al CODBARRAS EAN13( quedará 12)
                string codBarras = txtCodBarras.Text.Substring(0, txtCodBarras.Text.Length - 1);
                //con la cadena editada, "pintamos" el código de barras en el picBox
                picBoxCodBarras.Image = barcode.Draw(codBarras, 50);
            }
        }

        private void txtCodBarras_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
