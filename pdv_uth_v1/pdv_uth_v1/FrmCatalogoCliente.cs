﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoCliente : Form
    {

        //vars para acciones CRUD
        Cliente cli = new Cliente();
        int idCliente = 0;
            

        public FrmCatalogoCliente()
        {
            InitializeComponent();
        }

        private void FrmCatalogoCliente_Load(object sender, EventArgs e)
        {
            //vamos actualizar el dgClientes, con los registro de BD
            mostrarRegistrosEnDG();
        }

        private void mostrarRegistrosEnDG()
        {
            //borrar todos los ren del DG
            dgClientes.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo=" 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgClientes.DataSource= cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgClientes.Refresh();
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Cliente paraAlta = new Cliente();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCalle.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = txtComprobanteDomicilio.Text;
            paraAlta.ComprobanteINE = txtComproINE.Text;
           //falta curp y comrpobante curp
            if (paraAlta.alta())
            {
                MessageBox.Show("registro correcto");
                mostrarRegistrosEnDG();
            }
            else
            {
                MessageBox.Show("Errror al guardar cliente. "+Cliente.msgError);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Cliente paraModif = new Cliente();
           

            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();
            DatosParaActualizar dato = new DatosParaActualizar();
            dato.campo = "nombre";
            dato.valor= txtNombre.Text;
            datos.Add(dato);
            
            dato.campo = "apellido_paterno";
            dato.valor = txtApPat.Text;
            datos.Add(dato);

            dato.campo = "apellido_materno";
            dato.valor = txtApMat.Text;
            datos.Add(dato);

            dato.campo = "celular";
            dato.valor = txtCelular.Text;
            datos.Add(dato);

            dato.campo = "correo";
            dato.valor = txtCorreo.Text;
            datos.Add(dato);
            dato.campo = "fecha_de_nacimiento";
            dato.valor = dtpFechaNacimiento.Value.Year+"-"+dtpFechaNacimiento.Value.Month+"-"+dtpFechaNacimiento.Value.Day;
            datos.Add(dato);
            dato.campo = "calle";
            dato.valor = txtCalle.Text;
            datos.Add(dato);
            dato.campo = "numero_casa";
            dato.valor = txtNumCalle.Text;
            datos.Add(dato);
            dato.campo = "colonia";
            dato.valor = txtColonia.Text;
            datos.Add(dato);
            dato.campo = "codigo_postal";
            dato.valor = txtCP.Text;
            datos.Add(dato);
            dato.campo = "localidad";
            dato.valor = txtLocalidad.Text;
            datos.Add(dato);
            dato.campo = "municipio";
            dato.valor = txtMunicipio.Text;
            datos.Add(dato);
            dato.campo = "img_comprobante_domicilio";
            dato.valor = txtCalle.Text;
            datos.Add(dato);
            dato.campo = "ine_comprobante";
            dato.valor = txtComproINE.Text;
            datos.Add(dato);


            if (paraModif.modificar(datos, idCliente))
            {
                MessageBox.Show("cliente modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error cliente no cmodificado");
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.eliminar(idCliente))
                {
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error Cliente NO eliminado ");
            }
        }

        private void dgClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idCliente = int.Parse(dgClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgClientes.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCelular.Text = dgClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
           // txtTelefono.Text = dgClientes.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCalle.Text = dgClientes.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtNumCalle.Text = dgClientes.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtCP.Text = dgClientes.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtColonia.Text = dgClientes.Rows[e.RowIndex].Cells[10].Value.ToString();
            //txtFraccionamiento.Text = dgClientes.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtLocalidad.Text = dgClientes.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtMunicipio.Text = dgClientes.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtComprobanteDomicilio.Text = dgClientes.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtComproINE.Text = dgClientes.Rows[e.RowIndex].Cells[15].Value.ToString();
            //txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[16].Value.ToString();
            //txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[17].Value.ToString();
            //txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[18].Value.ToString();
        
        }
    }
}
