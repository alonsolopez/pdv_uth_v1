
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibBD;

namespace Lib_pdv_uth_v1.usuarios
{

    public class Usuario : Persona, ICrud
    {
        //Var de BD
        LibMySql bd;
        //var de errores
        public static string msgError = "";
        //constructor default
        public Usuario(): base(1, "", "",  new DateTime(), "", "","", "", new Domicilio(), "", "", "" )
        {
            bd = new LibMySql();
        }
        //props unicas de user
        private object actaNacimientoComprobante;
        private object cartaNoAntecedentesP;
        private object certificadoEscolar;
        private string numeroSeguroSocial;
        private TipoUsuario tipoUsuario;
        private string contraseña;

        //getters y Setters de Usuario
        public object ActaNacimientoComprobante { get => actaNacimientoComprobante; set => actaNacimientoComprobante = value; }
        public object CartaNoAntecedentesP { get => cartaNoAntecedentesP; set => cartaNoAntecedentesP = value; }
        public object CertificadoEscolar { get => certificadoEscolar; set => certificadoEscolar = value; }
        public string NumeroSeguroSocial { get => numeroSeguroSocial; set => numeroSeguroSocial = value; }
        public TipoUsuario TipoUsuario { get => tipoUsuario; set => tipoUsuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }



        /// <summary>
        /// @return
        /// </summary>
        public bool alta()
        {
            bool res = false;
            //--------------------------------------
            string fechaCorrecta = this.FechaNacimiento.Year + "-" + this.FechaNacimiento.Month + "-" + this.FechaNacimiento.Day;
            //los values de Usuario
            string valores = "'" + this.Nombre + "','" + this.ApellidoPaterno + "','" + this.ApellidoMaterno + "','" + this.Celular + "','" + this.Correo + "','" + fechaCorrecta + "','" + this.Domicilio.calle + "','" + this.Domicilio.numero + "','" + this.Domicilio.colonia + "','" + this.Domicilio.codigoPostal + "','" + this.Domicilio.localidad + "','" + this.Domicilio.municipio + "','" + this.Domicilio.fotoComprobante+ "','" + this.comprobanteINE + "'";
            if (bd.insertar("usuario",
                            "nombre, apellido_paterno,apellido_materno, celular, correo, fecha_de_nacimiento," +
                            "calle, numero_casa, colonia, codigo_postal, localidad, municipio, img_comprobante_domicilio, ine_comprobante",
                            valores))
            {
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta nuevo cliente. " + LibMySql.msgError;
            }
            //--------------------------------------
            return res;
        }

        /// <summary>
        /// @param datos 
        /// @param id 
        /// @return
        /// </summary>
        public bool modificar(Usuario datos, int id)
        {
            // TODO implement here
            return false;
        }

        /// <summary>
        /// @param id 
        /// @return
        /// </summary>
        public bool eliminar(int id)
        {
            // TODO implement here
            return false;
        }

        /// <summary>
        /// @param criteriosBusqueda 
        /// @return
        /// </summary>
        public List<Usuario> consultar(object criteriosBusqueda)
        {
            // TODO implement here
            return null;
        }

        /// <summary>
        /// Autenticaci{on de Usuario, mediante correo y constraseña.
        /// Si no se encuentra, llega TipoUsuario.ERROR!
        /// </summary>
        /// <param name="us">Correo del usuario a loguear</param>
        /// <param name="contraseña">Contraseña del usuario</param>
        /// <returns>TipoUsuario.ADMINISTRADOR, CAJERO o ERROR, según sea el caso</returns>
        public TipoUsuario login(string us, string contraseña)
        {
            //hacemos la consulta de un registro de usuario con correo y contraeeña
            object usLogueado = bd.consultarUnRegistro("*", "usuarios", " correo='" + us + "' AND pwd='" + contraseña + "'");
            object[] user = (object[])usLogueado;
            string tipo = user[22].ToString();
            //verificamos que haya un resultado
            if (usLogueado != null)
            {
                //el campo 22 es el tipo
                if (tipo == "ADMINISTRADOR")
                    return TipoUsuario.ADMINISTRADOR;
                else if (tipo == "CAJERO")
                    return TipoUsuario.CAJERO;
                else return TipoUsuario.ERROR;
            }
            else
            {
                msgError = "Usuario NO REGISTRADO. Consulte a Administrador. ";
                return TipoUsuario.ERROR;
            }
        }

        public bool modificar(List<DatosParaActualizar> datos, int id)
        {
            throw new NotImplementedException();
        }

        public List<T> consultar<T>(List<CriteriosBusqueda> criteriosBusqueda)
        {
            throw new NotImplementedException();
        }

        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            throw new NotImplementedException();
        }
    }
}