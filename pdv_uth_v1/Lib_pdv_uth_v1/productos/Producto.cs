﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib_pdv_uth_v1.usuarios;
using LibBD;

namespace Lib_pdv_uth_v1.productos
{
    public class Producto : ICrud
    {
        //props de producto
        private int id;
        private string nombre;
        private string descripcion;
        private double precio;
        private string codigoDeBarras;
        private string imagen;
        private UnidadDeMedida unidadDeMedida;
        private bool esPerecedero;

        public int Id { get => this.id; set => this.id = value; }
        public string Nombre { get => this.nombre; set => this.nombre = value; }
        public string Descripcion { get => this.descripcion; set => this.descripcion = value; }
        public double Precio { get => this.precio; set => this.precio = value; }
        public string CodigoDeBarras { get => this.codigoDeBarras; set => this.codigoDeBarras = value; }
        public string Imagen { get => this.imagen; set => this.imagen = value; }
        public UnidadDeMedida UnidadDeMedida { get => this.unidadDeMedida; set => this.unidadDeMedida = value; }
        public bool EsPerecedero { get => this.esPerecedero; set => this.esPerecedero = value; }
        //VAR DE ERROR
        public static string msgError;

        //VAR DE bd
        LibMySql bd;

        public Producto()
        {
            //mi bd en nube
            bd = new LibMySql("174.136.53.235", "aloncell_pdvuth", "gBH3SZ*CbGdz", "aloncell_pdvuth_bd");
        }
        //constructores

        //m{etodos Icrud 
        /// <summary>
        /// Registra nuevo producto, con los datos quue ya deben estar en los campos del obj prod
        /// </summary>
        /// <returns>true si se insertar, false si hay error (detalles del error en Producto.msgError)</returns>
        public bool alta()
        {
            bool res = false;
            string valores = "'" + this.nombre + "','" + descripcion + "'," + precio + ",'" + codigoDeBarras + "','" + imagen + "','" + unidadDeMedida + "'," + esPerecedero + "";
            if (bd.insertar("productos",
                            "nombre, descripcion,precio, codigo_barras, imagen_producto, unidad_medida, es_perecedero",
                            valores))
            {
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta nuevo Producto. " + LibMySql.msgError;
            }
            return res;
        }

       
        /// <summary>
        /// Elimina un producto del catálogo de productos, por su ID (no por código de barras).
        /// </summary>
        /// <param name="id">El ID del producto que queremos eliminar</param>
        /// <returns>true si se elimina, false si hay error (consultar Producto.msgError)</returns>
        public bool eliminar(int id)
        {
            bool res = false;
            if (bd.eliminar("productos", "id="+id))
                res = true;
            else
                msgError = "Error al dar de baja Producto ID ("+id+"). " + LibMySql.msgError;
            return res;
        }
        /// <summary>
        /// Actualiza la información de productos registrados. Cada cambio debe ser agregado a la lista 'datos'.
        /// </summary>
        /// <param name="datos">Lista de los cambios, con elementos de tipo DatosParaActualizar, par (campos,valor)</param>
        /// <param name="id">ID del producto que se actualizará</param>
        /// <returns>true si se actualizar, false, si hay error (ver detlle de error en 'Producto.msgError'.</returns>
        public bool modificar(List<DatosParaActualizar> datos, int id)
        {
            bool res = false;
            //crear la lista de datos actualizar
            string setQuery = "";
            foreach (DatosParaActualizar dato in datos)
                setQuery += dato.campo + "='" + dato.valor + "',";
            //borramos la última ','
            setQuery.Remove(setQuery.Length - 1, 1);
            //hacemos el query
            if (bd.actualizar("productos", setQuery, "id=" + id))
                res = true;
            else
                msgError = "Error al modificar producto. " + LibMySql.msgError;
            //devolver resultado
            return res;
        }
        /// <summary>
        /// Consulta la información de productos en general, puede ser uno o N productos resultantes. 
        /// Se utiliza lista de CriteriosDeBusqueda para el WHERE
        /// </summary>
        /// <param name="criteriosBusqueda">Lista de condiciones que va en el WHERE</param>
        /// <returns>Lista de Productos</returns>
        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //lista de object para resultado
            List<object> res = new List<object>();
            //hacemos el where
            string where = "";
            //interpretamos los operadores de cada condiciones del WHERE
            for (int i = 0; i < criteriosBusqueda.Count; i++)
                where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "productos", where);
            //mapeamos cada Object en un Cliente
            foreach (object[] prodTmp in tmp)
            {
                var prod = new
                {
                    Id = int.Parse(prodTmp[0].ToString()),
                    Nombre = prodTmp[1].ToString(),
                    Descripcion = prodTmp[2].ToString(),
                    Precio = double.Parse(prodTmp[3].ToString()),
                    CodigoDeBarras = prodTmp[4].ToString(),
                    Imagen = prodTmp[5].ToString(),
                    UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), prodTmp[6].ToString()),
                    EsPerecedero = prodTmp[7].ToString() == "1" ? true : false
                };
                //se agrega este prod a la lista e productos
                res.Add(prod);
            }
            //regresamos la lista de cliente
            return res;
        }
        /// <summary>
        /// Consulta por Código de BArras para el dataGrid de Caja.
        /// </summary>
        /// <param name="codBarras">El producto escaneado o capturado por su código de barras</param>
        /// <returns>Un objeto producto con todos o NULL si no se encontr{o (ver Producto.msgError)</returns>
        public Producto consultarPorCodigoDeBarras(string codBarras)
        {
            //consulta de un solo registro (el prod del codBarras que se escaneó)
            object[] prodTempArray = bd.consultarUnRegistro("*", "productos", " codigo_barras='" + codBarras + "'");
            if (prodTempArray != null)//si hay registro resultante
            {
                this.Id = int.Parse(prodTempArray[0].ToString());
                this.Nombre = prodTempArray[1].ToString();
                this.Descripcion = prodTempArray[2].ToString();
                this.Precio = double.Parse(prodTempArray[3].ToString());
                this.CodigoDeBarras = prodTempArray[4].ToString();
                this.Imagen = prodTempArray[5].ToString();
                this.UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), prodTempArray[6].ToString());
                this.EsPerecedero = prodTempArray[7].ToString() == "1" ? true : false;
            }
            else
            {
                msgError = "Error Producto con cod Barras <"+codBarras+">, no existe." +LibMySql.msgError;
            }
            return this;
        }//consultaPorCodBarras 


    }
}
