﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1
{
    public enum OperadorDeConsulta
    {
        AND,
        OR,
        LIKE, 
        IGUAL,
        DIFERENTE,
        NO_IGUAL,
        NINGUNO
    }
}
